(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")))

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package exec-path-from-shell
  :ensure t
  :init
  (exec-path-from-shell-initialize))


(require 'server)
(unless (server-running-p)
  (server-start))    ;use as a server so clients can connect

(set-face-attribute 'default nil
                    :family "Inconsolata"
                    :height 140
                    :weight 'regular)

;; Basic config
(setq modifier-keys-are-sticky t)
(setq inhibit-startup-message t) ;inhibit startup message
(setq initial-scratch-message nil) ;remove scratch message
(scroll-bar-mode -1) ;no scrollbar
(tooltip-mode -1) ;no tooltips

(add-to-list 'default-frame-alist '(fullscreen . maximized)) ;start maximized

(global-linum-mode) ;turn on line numbering

(global-prettify-symbols-mode +1) ;turn on pretty symbols

(setq mac-command-modifier 'meta) ;command key as meta

(column-number-mode) ;show column number in status

(add-hook 'before-save-hook 'delete-trailing-whitespace) ;delete trailing whitespaces on save

(setq ring-bell-function 'ignore) ;stop bell from ringing
(setq visible-bell nil) ;stop visual bell from showing

(global-auto-revert-mode) ;auto update external changes

(global-hl-line-mode) ;highlight current line

(setq display-time-24hr-format t)
(display-time-mode)
(display-battery-mode)

(global-set-key (kbd "C-x C-b") 'ibuffer)

(setq custom-safe-themes t)

(setq-default cursor-type 'box)
(global-visual-line-mode)

(ido-mode t)
(ido-everywhere t)

(setq abbrev-file-name "~/.emacs.d/abbrev_defs")
(setq save-abbrevs 'silent)        ;; save abbrevs when files are saved
(setq-default abbrev-mode t)

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files

(set-language-environment "UTF-8")

;; Packages
(use-package better-defaults
  :ensure t)

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))

(use-package org
  :ensure t
  :bind
  (
   ("C-c a" . org-agenda)
   ("C-c l" . org-store-link)
   ("C-c c" . org-capture)
   ("C-c b" . org-iswitchb))
  :init
  (add-hook 'org-mode-hook #'org-indent-mode)
  (setq org-log-done t)
  (setq org-log-reschedule t)
  (setq org-log-into-drawer t)
  (setq org-agenda-files '("~/Dropbox/org"))
  (setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/Dropbox/org/gtd.org" "Inbox")
         "* TODO %?\n  %i")
        ("j" "Journal" entry (file+datetree "~/Dropbox/org/journal.org")
         "* %?\nEntered on %U\n  %i")
        ("d" "Daily" checkitem (file+datetree+prompt "~/Dropbox/org/daily.org")
         "[ ] %?" :prepend t :kill-buffer t)))
  :config
  )

(use-package latex-preview-pane
  :ensure t
  :init

  :config
  (latex-preview-pane-enable))

;; (use-package auto-complete
;;   :ensure t
;;   :init
;;   (setq ac-show-menu-immediately-on-auto-complete t)
;;   :config
;;   ;; (setq ac-modes (delq 'python-mode ac-modes))
;;   (ac-config-default))

(use-package aggressive-indent
  :ensure t
  :init

  :config
  (global-aggressive-indent-mode 1))

(use-package mwim
  :ensure t
  :bind (
         ("C-a" . mwim-beginning)
         ("C-e" . mwim-end))
  :init

  :config
  )

(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :init
  :config
  (setq flycheck-check-syntax-automatically '(mode-enabled save)))

(use-package expand-region
  :ensure t
  :bind (
         ("C-=" . 'er/expand-region))
  :init

  :config)

(use-package py-autopep8
  :ensure t
  :init
  :config)

(use-package ein
  :ensure t
  :init
  :config)

(use-package sphinx-doc
  :ensure t
  :diminish sphinx-doc-mode
  :init
  :config)

(use-package company
  :ensure t
  :init

  :config
  )

(use-package elpy
  :ensure t
  :diminish elpy-mode
  :init
  (add-hook 'python-mode-hook #'sphinx-doc-mode)
  :config
  (remove-hook 'elpy-modules 'elpy-module-flymake)
  ;; (remove-hook 'elpy-modules 'elpy-module-highlight-indentation)
  (add-hook 'elpy-mode-hook 'flycheck-mode)
  (elpy-set-test-runner 'elpy-test-pytest-runner)
  (elpy-enable))

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status))
  :config
  )



(use-package multiple-cursors
  :ensure t
  :bind (
         ("C-S-c C-S-c" . mc/edit-lines)
         ("C->" . mc/mark-next-like-this)
         ("C-c C->" . mc/mark-all-like-this)))

;; (use-package neotree
;;   :ensure t
;;   :bind (("<f8>" . neotree-project-dir))
;;   ;;:bind (("<f8>" . neotree-toggle))
;;   :init
;;   (defun neotree-project-dir ()
;;     "Open NeoTree using the git root."
;;     (interactive)
;;     (condition-case nil
;;         (let ((project-dir (projectile-project-root))
;;            (file-name (buffer-file-name)))
;;        (if project-dir
;;            (if (neotree-toggle)
;;                (progn
;;                  (neotree-dir project-dir)
;;                  (neotree-find file-name)))
;;          ;;(message "Could not find git project root.")
;;          (neotree-toggle)))
;;         (error (neotree-toggle))))
;;   (setq neo-smart-open t)
;;   (setq projectile-switch-project-action 'neotree-projectile-action)
;;   )

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init

  :config
  (projectile-global-mode))

(use-package smex
  :ensure t
  :bind (
         ("M-x" . smex)
         ("M-X" . smex-major-mode-commands)
         ("C-c C-c M-x" . execute-extended-command))
  :init

  :config
  )

(use-package lua-mode
  :ensure t
  :bind (:map lua-mode-map
         ("C-c C-c" . lua-send-current-line)
         ("C-c C-r" . lua-send-region))
  :init
  (autoload 'lua-mode "lua-mode" "Lua editing mode." t)
  (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
  (add-to-list 'interpreter-mode-alist '("lua" . lua-mode))
  (add-hook 'lua-mode-hook 'flymake-mode)
  (setq lua-default-application "~/torch/install/bin/th")
  :config)

(use-package ghc
  :ensure t
  :init

  :config
  (autoload 'ghc-init "ghc" nil t)
  (autoload 'ghc-debug "ghc" nil t)
  (add-hook 'haskell-mode-hook (lambda () (ghc-init)))
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode))

(use-package tex
  :ensure auctex
  :init
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (setq reftex-plug-into-AUCTeX t))

(use-package avy
  :ensure t
  :bind (
         ("C-:" . avy-goto-char)
         ("C-;" . avy-goto-word-1)
         ("M-g l" . avy-goto-line))
  :init

  :config
  (avy-setup-default))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :init
  :config
  (which-key-mode))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "markdown"))

(use-package rainbow-delimiters
  :ensure t
  :init

  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package smartparens-config
  :ensure smartparens
  :diminish smartparens-mode
  :init

  :config
  (smartparens-global-strict-mode t))

(use-package flx
  :ensure flx-ido
  :init
  :config
  (flx-ido-mode 1)
  (setq ido-enable-flex-matching t)
  (setq ido-use-faces nil))

;; (use-package paredit
;;   :ensure t
;;   :init

;;   :config
;;   (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
;;   (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
;;   (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
;;   (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
;;   (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
;;   (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
;;   (add-hook 'scheme-mode-hook           #'enable-paredit-mode))

(use-package geiser
  :ensure t
  :init

  :config
  (setq geiser-active-implementations '(guile))
  )

(use-package ace-window
  :ensure t
  :bind
  (("C-x o" . ace-window))
  :init
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :config
  )

(add-to-list 'auto-mode-alist '("\\.m$" . octave-mode))

;; (use-package ox-reveal
;;   :ensure t
;;   :init
;;   ;;(setq org-reveal-root "file:///home/datah4ck3r/slides/reveal.js")
;;   (setq org-reveal-root "http://origin.jsdelivr.net/reveal.js/2.6.2/")
;;   (setq org-reveal-title-slide nil)
;;   :config)

(use-package git-gutter
  :ensure t
  :diminish git-gutter-mode
  :init
  :config
  (global-git-gutter-mode)
  (git-gutter:linum-setup))

;; Themes
;;=============
(use-package color-theme-sanityinc-tomorrow
  :ensure t
  :init

  :config
  ;;(load-theme 'sanityinc-tomorrow-day t)
  (load-theme 'sanityinc-tomorrow-night t)
  ;;(load-theme 'sanityinc-tomorrow-blue t)
  ;;(load-theme 'sanityinc-tomorrow-bright t)
  ;;(load-theme 'sanityinc-tomorrow-eighties t)
  )

;; Emacs 25 selected-package issue
(setq custom-file "~/.emacs.d/package-selected-packages.el")
(load custom-file)
