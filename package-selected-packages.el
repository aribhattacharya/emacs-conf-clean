(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (zenburn-theme doom-themes abbrev-mode abbrev flx-ido flx smartparens mwim aggressive-indent smex expand-region counsel color-theme-sanityinc-tomorrow git-gutter ace-window geiser paredit rainbow-delimiters markdown-mode which-key avy auctex ghc lua-mode projectile neotree multiple-cursors magit elpy company sphinx-doc ein py-autopep8 flycheck latex-preview-pane yasnippet better-defaults exec-path-from-shell use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
